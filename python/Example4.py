# Copyright (C) 2022 Texas Instruments Incorporated
#
#
# Redistribution and use in source and binary forms, with or without 
# modification, are permitted provided that the following conditions 
# are met:
#
#   Redistributions of source code must retain the above copyright 
#   notice, this list of conditions and the following disclaimer.
#
#   Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in the 
#   documentation and/or other materials provided with the   
#   distribution.
#
#   Neither the name of Texas Instruments Incorporated nor the names of
#   its contributors may be used to endorse or promote products derived
#   from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from TI_API_Suite import TI_Orders, TI_FinancialDocuments

# Introduction.
print("---Example 4---")
print("Here, we will pass our user credentials to access the Orders API and the Financial Document Retrieve API.")
print("This does not require you to manually request and pass an access token,")
print("because the Python module is designed to abstract this process from you.")
print("Please change lines 42 and 43 to reflect your user credentials.")

# Store user credentials and server URL address.
client_id = "<client_id>"
client_secret = "<client_secret>"
server = "https://transact.ti.com"
verify_ssl = True

# Confirm to user.
print("\nLogging into the financial document retrieve API at {} with ID {}.".format(server, client_id))
findoc = TI_FinancialDocuments(server=server, client_id=client_id, client_secret=client_secret, verify=verify_ssl)
print("We should now have access to the API!")

# Step 1: Get list of all orders.
print("\nBy default, we're not going to get a list of all the orders.")
print("But you can look at line below to see which function to call in order to do that.")
# response = cart.get_orders(verify=verify_ssl)

# This step no longer works because you cannot retrieve details for a mock order at this time.
# However, you can use the code below to retrieve details for an actual order.
# print("\nWe are actually going to get info from our mock order from example 2 or 3.")
# print("Please replace line 60 below with your mock order number from before.")
# orderNumber = "ORDER NUMBER HERE"
# response = cart.get_order_details(orderNumber=orderNumber, verify=verify_ssl)
# print("Response from server:")
# print(response.text)

print("\nNow, we're going to use (mock) commercial invoice information.")
orderNumber = 'T10999999'
invoiceNumber = '5999999999'
print("Our mock order number is {}, and our invoice number is {}.".format(orderNumber, invoiceNumber))

response = findoc.retrieve_financial_document_test(verify=verify_ssl)
# print(response.text)
print("We are not going to print the encoded PDF, but the request should have succeeded!")

# Conclude.
print("\nThat's all, folks!")