# Copyright (C) 2022 Texas Instruments Incorporated
#
#
# Redistribution and use in source and binary forms, with or without 
# modification, are permitted provided that the following conditions 
# are met:
#
#   Redistributions of source code must retain the above copyright 
#   notice, this list of conditions and the following disclaimer.
#
#   Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in the 
#   documentation and/or other materials provided with the   
#   distribution.
#
#   Neither the name of Texas Instruments Incorporated nor the names of
#   its contributors may be used to endorse or promote products derived
#   from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from TI_API_Suite import TI_Orders, TI_Inventory

# Introduction.
print("---Example 2---")
print("Here, we will pass our user credentials to access the Orders API and the Inventory & Pricing API.")
print("This does not require you to manually request and pass an access token,")
print("because the Python module is designed to abstract this process from you.")
print("Please change lines 42 and 43 to reflect your user credentials.")

# Store user credentials and server URL address.
client_id = "<client_id>"
client_secret = "<client_secret>"
server = "https://transact.ti.com"
verify_ssl = True

# Confirm to user.
print("\nLogging into Inventory & Pricing API {} with ID {}.".format(server, client_id))
catalog = TI_Inventory(server=server, client_id=client_id, client_secret=client_secret, verify=verify_ssl)

# Step 1: Get inventory of the OPN below.
opn  = "LM393DR"
print("\nFirst, we are using the inventory API to get how many items of OPN {} there are.".format(opn))
response = catalog.get_product_info(opn=opn, verify=verify_ssl)

# Step 2: View server response.
print("Response: There are {} of this part available.".format(response.json()["quantity"]))

# Step 3: Post an order to the TI store.
print("\nNext, we will post a (test) order to the TI store using the Orders API.")
print("Acquiring access to the API...")
cart = TI_Orders(server=server, client_id=client_id, client_secret=client_secret, verify=verify_ssl)
print("Please replace line 66 with your checkout profile ID!")
json = {
    "order": {
        "checkoutProfileId": "INSERT CHECKOUT PROFILE ID HERE",
        "customerPurchaseOrderNumber": "Example123",
        "endCustomerCompanyName":"INSERT END CUSTOMER NAME HERE",
        "expediteShipping":False,
        "customerOrderAttributes": [
            {
                "message": "MY TEST ORDER"
            }
        ],
        "lineItems": [
            {
                "customerOrderLineItemNumber": 1,
                "tiPartNumber": "NE555DR",
                "customerPartNumber": "MY CUSTOM PART NUMBER",
                "customReelIndicator": False,
                "quantity": 1,
                "customerItemComments": [
                    {
                        "message" : "SOME COMMENT GOES HERE"
                    }
                ]
            },
            {
                "customerOrderLineItemNumber": 2,
                "tiPartNumber": "LM358PWR",
                "quantity": 1,
            }
        ]
    }
}
response = cart.post_order_test(json=json, verify=verify_ssl)

# Step 4: View server response.
print("Response from server:")
print(response.text)

# Step 5: Get order details.
orderNumber = response.json()['orderInfo']['orderNumber']
print("\nOur mock order number is {}!".format(orderNumber))
print("Next we will try retrieving details about this order.")
response = cart.get_order_details(orderNumber=orderNumber, verify=verify_ssl)
print("Response from server:")
print(response.text)

# Conclude.
print("\nThat's all!")