This is a repository of utility tools and example scripts to make API requests to get or post data.

Please refer to [API Support](https://api-portal.ti.com/support) for additional help and resources.

# 1. Python API Requests

We have the following example modules in the `/python` directory, each corresponding to a slide in the API introduction slideshow.

- `Example1.py` retrieves an API access token.
- `Example2.py` retrieves inventory information and places a test store order.
- `Example3.py` subscribes to a part and places a test order when notified.
- `Example4.py` retrieves a list of placed orders and then retrieves each order's invoice.
- `Example5.py` retrieves a list of placed orders and then retrieves each order's ASN.

After updating the user credentials in the script, each example can be run from that directory in the terminal using the following command:

```
py <filename>.py
```

These examples use a module `TI_API_Suite.py` of API-specific classes which can also be used by customers out-of-the-box.

- `TI_AdvancedShippingNotifications` retrieves Advanced Shipping Notifications (ASN) and related information pertaining to an order.
- `TI_FinancialDocuments` retrieves financial documents pertaining to an order.
- `TI_Orders` allows the user to place and view orders.
- `TI_StoreCatalog` allows the user to browse the store inventory.
- `TI_StoreSubscription` allows the user to subscribe to parts in the store to receive notifications about them.

You can import specific classes from the above module using the following code in Python, replacing `<class>` with your desired API class:

```
from TI_API_Suite import <class>
```


Finally, the `API_Accessor.py` module contains the back-end logic for each of the API classes to acquire an API access token and send API requests.

You must have installed the `requests` package to use any of these classes and modules; it is available freely under the Apache2 license. The programs were written using Python 3.7.3.

## 1.1 Installing Python Packages

To make API requests on Python 3.7.3, you can use the latest version of the following packages:

- `requests` ([PyPi respository](https://pypi.org/project/requests/))

You can install a Python package using the command below, or you can manually build and install the program by downloading the source code from the indicated links above.

```
py -m pip install <package>
```

## 1.2 Writing Python Modules

Please refer to the list of examples above as a reference. You should include the following import statements in your module:

```
import requests
```

### 1.2.1 Get Request

To request data through the API, call `requests.get(url, headers)`:

- `url` is the URL for the API request.
- `headers` is a dictionary of HTTP headers to add onto the request.

To connect with the TI Store API, you will need an Apigee access token which lasts for 60 minutes. This token will be mapped to the `'Bearer'` attribute of the `headers` dictionary, along with any other HTTP headers you would like to pass on with the API request.

A basic API get request method can thus be written as below, where `access_token` represents your Apigee access token (see section 1.2.3):

```
def API_REQUEST_GET(self, url, headers = {}, access_token):
    headers['Bearer'] = access_token
    return requests.get(url=url, headers=headers)
```

The API response can be parsed in a variety of ways.

- `obj.text` returns the response body as text.
- `obj.content` returns the response body as bytes.
- `obj.json()` decodes the response body into a Python dictionary, assuming it is in JSON format.
- `obj.raw` returns the server's socket request.

If you expect to be working exclusively with one reading, you can 'hard-code' the conversion into the API request method. Below is an example where the response is always decoded as a JSON object into a dictionary.

```
def API_REQUEST_GET_JSON(self, url, headers = {}, access_token):
    headers['Bearer'] = access_token
    response = requests.get(url=url, headers=headers)
    return response.json()
```

### 1.2.2 Post Request

To post JSON data through the API, call `requests.post(url, auth, json)`:

- `url` is the URL for the API request.
- `auth` is an object of class `HTTPBasicAuth`. See section 1.2.1.
- `json` is the payload as a JSON object dictionary.

Please read documentation cited in section 1.3 to learn more about other possible payload formats.

A basic API post request method can thus be written as:

```
def API_REQUEST_POST(self, url, headers, access_token, json):
    headers['Bearer'] = access_token
    return requests.post(url=url, headers=headers, json=json)
```

The method returns a response from the server which can be read in the same way as the responses from section 1.2.1.

### 1.2.3 Apigee Access

In order to make API requests on the TI Store API, you must have an Apigee access token. You must request an access token through a different API call, posting a JSON payload with the following attributes:

- `grant_type`: This will be set to `client_credentials`.
- `client_id`: This is your TI connection ID.
- `client_secret`: This is your TI connection password.

After initializing the payload as a Python dictionary, you will pass it onto a call to `requests.post()`. Below is an example, assuming that `url` contains the URL you have been given to request your token.

```
def get_access_info(self, url, client_id, client_secret):
    
    # Payload object.
    payload = {
        'grant_type': 'client_credentials',
        'client_id': client_id,
        'client_secret': client_secret
    }

    # API post request.
    response = requests.post(url=url, json=payload)

    # Check if request was successful, on status code 200.
    if(response.status_code == 200):

        # If yes, get token data and return access information.
        return response.json()

    # Otherwise, the request has failed.
    return None
```

The above method would return a dictionary with the following keys:

- `issued_at`: The time at which the access token was created.
- `expires_in`: The duration for which the access token will remain valid.
- `access_token`: The access token itself.

You can add the values at `issued_at` and `expires_in` to find the date-time at which the access token will become invalid.

### 1.2.4 Other Tips

- The optional `verify` parameter for `requests.get()` and `requests.post()` is a boolean which indicates whether the method will verify the server's TLS certificate before sending the request.

## 1.3 Package Documentation

Please refer to the following documentation to learn more about how to use the `requests` package for Python.

- "[Quickstart](https://requests.readthedocs.io/en/latest/user/quickstart/)", *Requests 2.28.1 documentation*. Explains how to use the basic API request methods, in greater detail.
- "[Developer Interface](https://requests.readthedocs.io/en/latest/api/)", *Requests 2.28.1*. Explains the various parameters of the `get()` and `post()` methods.
- "[Authentication](https://requests.readthedocs.io/en/latest/user/authentication/), *Requests 2.28.1 documentation*. Explains how to interface with web services which require user authentication. Our API uses OAuth 1.