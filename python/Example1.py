# Copyright (C) 2022 Texas Instruments Incorporated
#
#
# Redistribution and use in source and binary forms, with or without 
# modification, are permitted provided that the following conditions 
# are met:
#
#   Redistributions of source code must retain the above copyright 
#   notice, this list of conditions and the following disclaimer.
#
#   Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in the 
#   documentation and/or other materials provided with the   
#   distribution.
#
#   Neither the name of Texas Instruments Incorporated nor the names of
#   its contributors may be used to endorse or promote products derived
#   from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from API_Accessor import API_Accessor

# Introduction.
print("---Example 1---")
print("Here, we will pass our user credentials to attempt to receive an OAuth 1 access token.")
print("Usually, with the API-specific modules, you will not have to worry about keeping track of the token.")
print("However, here you can see how these modules retrieve the token.")
print("Please change lines 42 and 43 to reflect your user credentials.")

# Store user credentials and server URL address.
client_id = "<client_id>"
client_secret = "<client_secret>"
server = "https://transact.ti.com"
verify_ssl = True

# Confirm to user.
print("\nLogging into {} with ID {}.".format(server, client_id))
api = API_Accessor(server=server, client_id=client_id, client_secret=client_secret, verify=verify_ssl)

# Print output.
print("Access token {} retrieved!".format(api.token))
print("Expires on {}.".format(api.expiration.strftime("%m/%d/%Y at %H:%M:%S")))

# Conclude.
print("\nAll done!")